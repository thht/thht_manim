from manim import ImageMobject
import os
import numpy as np
from PIL import Image


class CroppableImageMobject(ImageMobject):
    def __init__(self, file_name, **kwargs):
        im = Image.open(file_name).convert('RGBA')
        im = self.process_image(im)
        super().__init__(np.array(im), **kwargs)

    def process_image(self, im):
        return im


class PNGImage(CroppableImageMobject):
    def __init__(self, file_name, **kwargs):
        super().__init__(
            os.path.join('assets', 'png_images', '%s.png' % (file_name, ))
        )


class JPGImage(CroppableImageMobject):
    def __init__(self, file_name, **kwargs):
        super().__init__(
            os.path.join('assets', 'jpg_images', '%s.jpg' % (file_name, ))
        )
