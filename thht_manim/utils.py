import numpy as np
import array_to_latex as a2l
from manim import *
from thht_manim.numbers import MathTexWithOpaqueBackground


def reposition_arrow(arrow, start, end):
    arrow.start = start
    arrow.end = end

    if not np.all(start == end):
        arrow.set_points_as_corners([start, end])
        arrow.put_start_and_end_on(start, end)
        arrow.reset_points()
        arrow.generate_points()
        arrow.position_tip(arrow.tip)


def update_matrix_part(o, tex_idx, mat, a2l_kwargs=None):
    if isinstance(mat, np.ndarray):
        mat = a2l.to_ltx(mat, **a2l_kwargs)

    new_tex = SingleStringMathTex(mat)

    tex_o = o
    if isinstance(o, MathTexWithOpaqueBackground):
        tex_o = o.tex

    old_mo = tex_o.submobjects[tex_idx]
    new_tex.align_to(old_mo, LEFT)
    new_tex.set_y(old_mo.get_y())
    new_tex.match_color(old_mo)

    old_family = tex_o.get_family()
    tex_o.submobjects[tex_idx] = new_tex
    to_add = new_tex

    to_remove = old_mo

    if old_mo in old_family:
        old_mo.points[:] = 0
        for m in old_mo.get_family():
            m.points[:] = 0
        old_mo.submobjects = []

    if isinstance(o, MathTexWithOpaqueBackground):
        o.update_rect()

    return to_add, to_remove
