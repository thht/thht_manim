from manim import *


class ApplyMethodFaded(ApplyMethod):
    def interpolate_submobject(self, submob, start, target_copy, alpha):
        submob.points = straight_path(start.points, target_copy.points, 1)
        submob.interpolate_color(start, target_copy, alpha)

        return self

    def begin(self):
        # Use a copy of target_mobject for the align_data
        # call so that the actual target_mobject stays
        # preserved.
        self.target_mobject = self.create_target()
        self.check_target_mobject_validity()
        self.target_copy = self.target_mobject.copy()
        # Note, this potentially changes the structure
        # of both mobject and target_mobject
        self.mobject.align_data(self.target_copy)
        super().begin()
