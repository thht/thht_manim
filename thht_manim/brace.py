from manim import *


class BraceLabelBetterScale(BraceLabel):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.brace.put_at_tip(self.label, buff=DEFAULT_MOBJECT_TO_MOBJECT_BUFFER * self.label_scale)
