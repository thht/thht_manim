from manim import *
import numpy as np


class BetterVector(Arrow):
    def __init__(self, start, end, buff=0, **kwargs):
        self.startpoint = start
        self.endpoint = end
        super().__init__(start, start + RIGHT, buff=buff, **kwargs)

        self.set_points(endpoint=end)

    def set_points(self, startpoint=None, endpoint=None):
        if startpoint is None:
            startpoint = self.startpoint

        if endpoint is None:
            endpoint = self.endpoint

        new_length = max(np.linalg.norm(endpoint - startpoint), 0.01)
        try:
            new_angle = angle_of_vector(endpoint - startpoint)
        except TypeError:
            new_angle = 0

        self.shift(startpoint - self.startpoint)
        old_start = self.get_start()
        self.set_length(new_length)
        self.set_angle(new_angle)
        self.shift(old_start - self.get_start())

        self.startpoint = self.get_start()
        self.endpoint = self.get_end()

        return self

    def set_length(self, length):
        new_length = max(length, 0.01)

        return super().set_length(new_length)
