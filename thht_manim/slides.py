from manim import *
import markdown
from copy import deepcopy


class SlideHeader(Title):
    def __init__(self, *text_parts, max_width='25em', **kwargs):
        super().__init__(r'\parbox{%s}{' % (max_width, ), r'\centering\textbf{', *text_parts, r'}}', **kwargs)


class SlideText(Tex):
    def __init__(self, *text_parts,
                 scale_factor=0.8,
                 max_width='30em',
                 tex_template=None,
                 alignment=None, **kwargs):
        if 'markdown' in kwargs:
            del kwargs['markdown']
        if 'to_header_buff' in kwargs:
            del kwargs['to_header_buff']
        if 'center_align' in kwargs:
            del kwargs['center_align']

        tex_template = tex_template

        if tex_template is None:
            tex_template = deepcopy(config["tex_template"])

        tex_template.add_to_preamble(r'\setlength{\textwidth}{%s}' % (max_width, ))

        if alignment is not None:
            text_parts = (alignment + " ",  *text_parts)

        super().__init__(*text_parts, tex_template=tex_template, **kwargs)
        self.scale(scale_factor)
        self.to_edge(UP, buff=DEFAULT_MOBJECT_TO_EDGE_BUFFER * 3.5)
        self.to_edge(LEFT, buff=DEFAULT_MOBJECT_TO_EDGE_BUFFER * 2)


class SlideTextMarkdown(SlideText):
    def __init__(self, *text_parts, **kwargs):
        md = markdown.Markdown(extensions=['mdx_latex'])

        new_text_parts = [md.convert(x) for x in text_parts]

        if new_text_parts[0].startswith('<root>'):
            new_text_parts[0] = new_text_parts[0][6:]

        if new_text_parts[-1].endswith('</root>'):
            new_text_parts[-1] = new_text_parts[-1][0:-7]

        super().__init__(*new_text_parts, **kwargs)

