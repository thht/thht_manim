from manim import *
from thht_manim.animations import BetterSuccession
from thht_manim.slides import SlideHeader, SlideText, SlideTextMarkdown

from .ordered_cairo_renderer import OrderedCairoRenderer, SplitCairoRenderer


class DefaultSectionScene(object):
    def play(self, *args, add_new_scene=True, **kwargs):
        super().play(*args, **kwargs)

        if add_new_scene:
            self.next_section()



class AddExtraFrame(object):
    def get_time_progression(self, run_time, n_iterations=None, override_skip_animations=False):
        if write_to_movie():
            self.original_runtime = run_time
            run_time += 4*(1/self.renderer.camera.frame_rate)

        return super().get_time_progression(run_time, n_iterations, override_skip_animations)

    def update_animation_to_time(self, t):
        if write_to_movie():
            if t <= self.original_runtime:
                super().update_animation_to_time(t)

            self.last_t = t
        else:
            super().update_animation_to_time(t)


class SceneOrderedPartials(object):
    def setup(self):
        self.renderer = OrderedCairoRenderer(camera_class=self.camera_class)
        self.renderer.init(self)


class SplitScene(object):
    default_add_split_at_play = True

    def __init__(self, *args, camera_class=Camera,
                 skip_animations=False, **kwargs):
        kwargs['renderer'] = SplitCairoRenderer(
            camera_class=camera_class,
            skip_animations=skip_animations,

        )
        super().__init__(*args, camera_class=camera_class,
                         skip_animations=skip_animations,
                         **kwargs)

    def add_split(self):
        self.renderer.add_split()

    def play(self, *args, add_split=None, **kwargs):
        if add_split is None:
            add_split = self.default_add_split_at_play

        super().play(*args, **kwargs)
        if add_split:
            self.add_split()

    def get_animation_time_progression(self, animations):
        prog = super().get_animation_time_progression(animations)
        prog.set_description(
            "".join(
                [
                    "Animation {}/{}: ".format(self.renderer.file_writer._cur_split, self.renderer.num_plays),
                    str(animations[0]),
                    (", etc." if len(animations) > 1 else ""),
                ]
            )
        )

        return prog

    def get_wait_time_progression(self, duration, stop_condition):
        prog = super().get_wait_time_progression(duration, stop_condition)
        if stop_condition is None:
            prog.set_description(
                "Waiting {}/{}".format(self.renderer.file_writer._cur_split, self.renderer.num_plays)
            )


class ExtraWaitAfterAnimation(object):
    def play_internal(self, *args, **kwargs):
        new_animations = BetterSuccession(
            AnimationGroup(*args),
            FadeIn(Mobject(), run_time=0.2)
        )

        super().play_internal(new_animations, **kwargs)



class AddSlideNumber(object):
    def __init__(self, *args, slide_number_fontsize=24, **kwargs):
        super().__init__(*args, **kwargs)

        self._slide_number_counter = 0
        self._slide_number_object = None
        self.slide_number_fontsize = slide_number_fontsize

    def play(self, *args, **kwargs):
        self._update_slide_number()
        super().play(*args, **kwargs)

    def _update_slide_number(self):
        if self._slide_number_object is not None:
            self.remove(self._slide_number_object)

        self._slide_number_object = Text(str(self._slide_number_counter + 1), font_size=self.slide_number_fontsize)
        self._slide_number_object.to_corner(DOWN + RIGHT)
        self.add(self._slide_number_object)

    def next_section(self, *args, **kwargs):
        self._slide_number_counter += 1
        return super().next_section(*args, **kwargs)


class SlideScene(object):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self._slide_header = None
        self._slide_text = None

    def remove_header(self):
        if self._slide_header is not None:
            self.remove(self._slide_header)

    def set_header(self, *text, font_size=42, **kwargs):
        self.remove_header()
        self._slide_header = SlideHeader(*text, font_size=font_size, **kwargs)
        self._slide_header.to_edge(UP, buff=0.5*DEFAULT_MOBJECT_TO_EDGE_BUFFER)
        self.add(self._slide_header)

    def remove_text(self):
        if self._slide_text is not None:
            self.remove(self._slide_text)

    def set_text(self, *text,
                 to_header_buff=DEFAULT_MOBJECT_TO_MOBJECT_BUFFER*3,
                 markdown=True, center_align=False, add_to_scene=True,
                 **kwargs):
        self.remove_text()
        if markdown:
            self._slide_text = SlideTextMarkdown(*text, **kwargs)
        else:
            self._slide_text = SlideText(*text, **kwargs)

        if self._slide_header:
            self._slide_text.next_to(self._slide_header, DOWN,
                                     buff=to_header_buff, coor_mask=[0, 1, 0])

        if center_align:
            self._slide_text.set_x(0)

        if add_to_scene:
            self.add(self._slide_text)


class FixedSpecialThreeDScene(SpecialThreeDScene):
    def __init__(self, **kwargs):
        ThreeDScene.__init__(self, **kwargs)
