from manim import *
from manim.utils.family import extract_mobject_family_members


class FixedThreeDCamera(ThreeDCamera):
    def extract_mobject_family_members(self, *mobjects):
        return extract_mobject_family_members(*mobjects)