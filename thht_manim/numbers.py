from manim import *


class FlexibleVariable(Variable):
    def __init__(self, var, label, var_object, color=WHITE, **kwargs):
        self.value = var_object

        var_for_init = var
        if isinstance(var, complex):
            var_for_init = var.real

        super().__init__(var=var_for_init, label=label, var_type=None,
                         num_decimal_places=0, **kwargs)

        if isinstance(var, complex):
            self.tracker = ComplexValueTracker(var)

        self.label.set_color(color)


class ColoredComplexNumber(DecimalNumber):
    def __init__(self, number=0, real_color=WHITE,
                 imag_color=WHITE, **kwargs):
        self.real_color = real_color
        self.imag_color = imag_color

        super().__init__(number=number, **kwargs)

        self.set_colors()

    def set_colors(self):
        is_real = True
        is_first = True
        for char in self.submobjects:
            if isinstance(char, SingleStringMathTex):
                if char.tex_string in ('+', '-') and not is_first:
                    is_real = False
                    continue

                if is_real:
                    color = self.real_color
                else:
                    color = self.imag_color

                char.set_color(color)
                char.set_fill(color, 1)

            is_first = False

    def set_value(self, number: float):
        super().set_value(number)
        self.set_colors()

    # def set_value(self, number, **config):
    #     super().set_value(number)
    #     # Make sure last digit has constant height
    #     self.scale(self[-1].get_height() / new_decimal[-1].get_height())
    #     self.move_to(self, self.edge_to_fix)
    #
    #     old_family = self.get_family()
    #     self.submobjects = new_decimal.submobjects
    #     for mob in old_family:
    #         # Dumb hack...due to how scene handles families
    #         # of animated mobjects
    #         mob.points[:] = 0
    #     self.number = number
    #     return self


class MathTexWithOpaqueBackground(VMobject):
    def __init__(self, *tex_strings, background_color=BLACK,
                 background_opacity=1,
                 padding=None,
                 **kwargs):

        if padding is None:
            padding = dict(
                left=0,
                right=0,
                top=0,
                bottom=0
            )

        super().__init__(**kwargs)
        self.initial_config = kwargs

        self.tex = MathTex(*tex_strings, **kwargs)

        r_width = self.tex.get_width() + padding['left'] + padding['right']
        r_height = self.tex.get_height() + padding['top'] + padding['bottom']

        self.background = Rectangle(color=background_color,
                                    fill_color=background_color,
                                    fill_opacity=background_opacity,
                                    stroke_opacity=background_opacity,
                                    width=r_width,
                                    height=r_height)

        self.tex.align_to(self.background, -padding['bottom']*UP + padding['right'] * LEFT)

        self.add(self.background, self.tex)

    def update_rect(self):
        old_x = self.background.get_x(UP+LEFT)
        old_y = self.background.get_y(UP + LEFT)
        self.background.set_width(self.tex.get_width() + self.padding['left'] + self.padding['right'], stretch=True)
        self.background.set_height(
            self.tex.get_height() + self.padding['top'] + self.padding['bottom'], stretch=True)

        self.background.set_x(old_x, UP+LEFT)
        self.background.set_y(old_y, UP + LEFT)

    def set_tex(self, *new_tex):
        full_config = dict(self.CONFIG)
        full_config.update(self.initial_config)

        new_tex_o = MathTexWithOpaqueBackground(*new_tex, **full_config)
        new_tex_o.align_to(self, LEFT)
        new_tex_o.set_y(self.get_y())
        new_tex_o.tex.match_style(self.tex)
        new_tex_o.background.match_style(self.background)

        old_family = self.get_family()
        self.submobjects = new_tex_o.submobjects
        self.tex = new_tex_o.tex
        self.background = new_tex_o.background

        for mob in old_family:
            # Dumb hack...due to how scene handles families
            # of animated mobjects
            if not mob in self.get_family():
                mob.points[:] = 0
                mob.submobjects = []

        return self
