from manim import *
from thht_manim.numbers import MathTexWithOpaqueBackground
from thht_manim.utils import reposition_arrow, update_matrix_part

from .brace import BraceLabelBetterScale
import array_to_latex as a2l


class MatrixAsRect(Group):
    def __init__(self, mat, width=None, height=None,
                 color=RED, fill_color=RED, fill_opacity=1, vector_extra_width=10,
                 **kwargs):
        super().__init__(**kwargs)

        if height is None and width is None:
            raise ValueError('You must set at either height or width!')

        self._m = mat.shape[0]
        if len(mat.shape) == 2:
            self._n = mat.shape[1]
        else:
            self._n = 1

        h_to_w = self._m / self._n

        if height is None:
            height = width * h_to_w

        if width is None:
            width = height / h_to_w
            if self._n == 1:
                width *= vector_extra_width

        self._braces = None
        self._rect = Rectangle(
            width=width,
            height=height,
            color=color,
            fill_color=fill_color,
            fill_opacity=fill_opacity,
            stroke_width=0
        )

        self.add(self._rect)

        self.update_braces()

    def update_braces(self, text_scale_factor=1):
        self.remove(self._braces)

        self._braces = Group(
            BraceLabelBetterScale(self._rect, self._m, LEFT, label_scale=text_scale_factor, buff=0.2*text_scale_factor),
        )

        if self._n > 1:
            self._braces.add(
                BraceLabelBetterScale(self._rect, self._n, label_scale=text_scale_factor, buff=0.2*text_scale_factor)
            )

        self.add(self._braces)

        return self

    def get_rect_width(self):
        return self.submobjects[0].get_width()

    def get_rect_height(self):
        return self.submobjects[0].get_height()


class ImageFromMatrix(ImageMobject):
    def __init__(self, mat, **kwargs):
        mat = mat[:, :, np.newaxis].repeat(4, axis=2)
        mat[:, :, 3] = 255

        super().__init__(mat.astype('uint8'), **kwargs)


class ImageFromVector(ImageFromMatrix):
    def __init__(self, vector, transpose=False, scale_factor_width=1, **kwargs):
        mat = vector[:, np.newaxis]

        height = 2

        if transpose:
            mat = mat.T
            width = 2
            height = width / vector.shape[0]

        if 'height' not in kwargs:
            kwargs['height'] = height

        super().__init__(mat, **kwargs)

        if transpose:
            self.set_height(self.get_height() * scale_factor_width, stretch=True)
        else:
            self.set_width(self.get_width() * scale_factor_width,
                           stretch=True)


class ImageFromVectorAlignedToMatrixAsRect(ImageFromVector):
    def __init__(self, vector, rect_mat, transpose=False, offset=0, **kwargs):
        super().__init__(vector, transpose=transpose, **kwargs)

        if transpose:
            self.rescale_to_fit(rect_mat.get_rect_width(), 0, stretch=True)
            shift = (0, -offset / rect_mat._n, 0)
        else:
            self.rescale_to_fit(rect_mat.get_rect_height(), 1, stretch=True)
            shift = (offset / rect_mat._m, 0, 0)

        self.align_to(rect_mat._rect, UL)
        self.shift(shift)


class MatrixTracker(ValueTracker):
    def __init__(self, mat, **kwargs):
        Mobject.__init__(self, **kwargs)
        self._original_shape = mat.shape
        self._n_elements = mat.size

        self.points = np.zeros((self._n_elements, 3))

        self.set_value(mat)

    def set_value(self, mat):
        mat_flat = mat.flatten()
        self.points[:, 0] = mat_flat

        return self

    def get_value(self):
        mat_flat = self.points[:, 0]

        return mat_flat.reshape(self._original_shape)

    def set_single_value(self, idx, val):
        mat = self.get_value()
        mat.itemset(idx, val)

        self.set_value(mat)

    def get_latex(self, frmt='{:.2f}'):
        return a2l.to_ltx(self.get_value(),
                          row=False,
                          print_out=False,
                          frmt=frmt)


class MatrixVariable(VMobject):
    def __init__(self, label, initial_value, a2l_kwargs=None, mathtex_kwargs=None,
                 color=WHITE, **kwargs):
        super().__init__(**kwargs)

        if a2l_kwargs is None:
            a2l_kwargs = dict(row=False,
                              print_out=False,
                              frmt='{:.2f}')

        if mathtex_kwargs is None:
            mathtex_kwargs = dict(
                background_opacity=0.5
            )

        self.a2l_kwargs = a2l_kwargs

        self.tracker = MatrixTracker(initial_value)
        self.formula = MathTexWithOpaqueBackground(
            r'%s =' % (label,), self._tracker_to_ltx(),
            substrings_to_isolate=[r'%s =' % (label,)],
            **mathtex_kwargs
        )
        self.formula.tex.set_color(color)

        self.formula.add_updater(
            lambda o: o.set_tex(r'%s =' % (label,), self._tracker_to_ltx())
        )

        self.add(self.formula)

    def _tracker_to_ltx(self):
        return a2l.to_ltx(self.tracker.get_value(), **self.a2l_kwargs)


class RotationMatrixVariable(MatrixVariable):
    def __init__(self, label, initial_value, **kwargs):
        self.angle_tracker = ValueTracker(initial_value)

        super().__init__(label, rotation_matrix(self.angle_tracker.get_value(), axis=OUT)[:2, :2], **kwargs)

        self.add_updater(
            lambda o: o.tracker.set_value(rotation_matrix(self.angle_tracker.get_value(), axis=OUT)[:2, :2])
        )

    def _tracker_to_ltx(self):
        angle = self.angle_tracker.get_value()

        ltx = r'''
\begin{bmatrix}
\cos(%.2f) & -\sin(%.2f) \\
\sin(%.2f) & \cos(%.2f)
\end{bmatrix}        
''' % (angle, angle, angle, angle)

        return ltx


class TrackerVectorWithFormulaAndArrow(VMobject):
    def __init__(self, coord_system, color=WHITE, initial_value=np.array([0, 0]),
                 label=r'\vec{v}', a2l_kwargs=None,
                 mathtex_kwargs=None,
                 arrow_color=None, **kwargs):
        super().__init__(**kwargs)

        if a2l_kwargs is None:
            a2l_kwargs = dict(row=False,
                              print_out=False,
                              frmt='{:.2f}')

        if mathtex_kwargs is None:
            mathtex_kwargs = dict(
                background_opacity=0.5
            )

        if arrow_color is None:
            arrow_color = color

        self.variable = MatrixVariable(label, initial_value, a2l_kwargs, mathtex_kwargs, color=color)
        self.formula = self.variable.formula

        self.tracker = self.variable.tracker
        self.arrow = coord_system.get_vector(self.tracker.get_value(), color=arrow_color)

        def update_arrow(o):
            reposition_arrow(o, coord_system.c2p(0, 0), coord_system.c2p(*self.tracker.get_value()))
            return o

        self.arrow.add_updater(update_arrow)
        self.add(self.variable, self.arrow)
