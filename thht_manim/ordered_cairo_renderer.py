from manim import CairoRenderer
from .scene_file_writer import OrderedSceneFileWriter, SplitSceneFileWriter


class OrderedCairoRenderer(CairoRenderer):
    def __init__(self, *args, **kwargs):
        super().__init__(
            file_writer_class=OrderedSceneFileWriter,
            *args, **kwargs
        )


class SplitCairoRenderer(CairoRenderer):
    def __init__(self, *args, **kwargs):
        super().__init__(
            file_writer_class=SplitSceneFileWriter,
            *args, **kwargs
        )

    def add_split(self):
        self.file_writer.add_split()
        self.num_plays = 0

    def scene_finished(self, *args):
        self.num_plays = 1
        return super().scene_finished(*args)
