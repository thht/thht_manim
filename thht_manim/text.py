from manim import *


class DefaultText(Text):
    def __init__(self, *args, font='Droid Sans', **kwargs):
        super().__init__(*args, font=font, **kwargs)