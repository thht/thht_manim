from manim import *


class ThreeDSystem(ThreeDAxes):
    def get_vector(self, coords, start_coords=(0, 0, 0), **kwargs):
        kwargs["buff"] = 0
        v = Arrow(
            self.coords_to_point(*start_coords), self.coords_to_point(*coords), **kwargs
        )

        v.add_updater(
            lambda v: v.position_tip(v.tip)
        )

        return v

    def get_line(self, coords, start_coords=(0, 0, 0), **kwargs):
        return Line(self.c2p(*start_coords), self.c2p(*coords), **kwargs)

    @property
    def current_rotation(self):
        return np.array([self.c2p(1, 0, 0), self.c2p(0, 1, 0), self.c2p(0, 0, 1)]).T

    @current_rotation.setter
    def current_rotation(self, rot):
        rot_dif = rot @ np.linalg.inv(self.current_rotation)

        self.apply_matrix(rot_dif, about_point=self.c2p(0, 0, 0))

    def begin_3dillusion_camera_rotation(self, rate=1):
        self._start_rotation = self.current_rotation

        val_tracker = ValueTracker(0)

        def update_rot(v, dt):
            val_tracker.increment_value(dt * rate)
            val_left_right = 0.2 * np.sin(val_tracker.get_value())
            val_up_down = 0.1 * np.cos(val_tracker.get_value())

            rot_matrix = rotation_matrix(val_left_right, RIGHT) @ rotation_matrix(val_up_down, UP) @ self._start_rotation

            self.current_rotation = rot_matrix

        self.add_updater(update_rot)

        self._3d_tracker = update_rot

    def stop_3dillusion_camera_rotation(self):
        self.remove_updater(self._3d_tracker)


class BetterNumberPlane(NumberPlane):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        self.cur_ij_hat_mat = np.eye(2)

    def get_vector(self, coords, start_coords=(0, 0), **kwargs):
        kwargs["buff"] = 0

        return Arrow(self.c2p(*start_coords), self.c2p(*coords), **kwargs)

    def set_i_hat_j_hat(self, i_hat, j_hat):
        mat = np.array([i_hat, j_hat]).T
        if np.linalg.matrix_rank(mat) < 2:
            return

        trans_mat = mat @ np.linalg.inv(self.cur_ij_hat_mat)

        if np.linalg.matrix_rank(trans_mat) < 2:
            return

        self.apply_matrix(trans_mat, about_point=self.c2p(0, 0, 0))
        self.cur_ij_hat_mat = mat