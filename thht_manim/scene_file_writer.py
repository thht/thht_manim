from manim.scene.scene_file_writer import SceneFileWriter
from manim import logger
import os
import glob
from pathlib import Path
import logging


class SplitSceneFileWriter(SceneFileWriter):
    def __init__(self, *args, **kwargs):
        self._cur_split = 0
        self._movie_file_path = None

        super().__init__(*args, **kwargs)

        self._partial_movie_files = [[]]

    @property
    def partial_movie_files(self):
        return self._partial_movie_files[self._cur_split]

    @partial_movie_files.setter
    def partial_movie_files(self, val):
        return

    def add_split(self):
        self._partial_movie_files.append([])
        self._cur_split += 1

    @property
    def movie_file_path(self):
        if self._movie_file_path is None:
            return None
        p = Path(self._movie_file_path)
        new_p = Path(p.parent, '%03d_%s' % (self._cur_split, p.name))
        return str(new_p)

    @movie_file_path.setter
    def movie_file_path(self, val):
        self._movie_file_path = val

    def combine_movie_files(self, partial_movie_files=None):
        n_splits = self._cur_split + 1

        for idx_split in range(n_splits):
            self._cur_split = idx_split
            if self.partial_movie_files:
                super().combine_movie_files()

    def init_output_directories(self, scene_name):
        super().init_output_directories(scene_name)

        if self.movie_file_path is not None:
            p = Path(self.movie_file_path)
            new_p = p.parent / scene_name
            new_p.mkdir(exist_ok=True)
            new_p /= p.name

            self.movie_file_path = str(new_p)

    def close_movie_pipe(self, *args, **kwargs):
        prev_level = logger.level
        if prev_level in (logging.INFO, 0):
            logger.setLevel(logging.ERROR)
        super().close_movie_pipe(*args, **kwargs)

        logger.setLevel(prev_level)

        logger.info(
            f"Animation {self._cur_split}/{self.renderer.num_plays} : Partial movie file written in %(path)s",
            {"path": {self.partial_movie_file_path}},
        )

class OrderedSceneFileWriter(SceneFileWriter):
    def add_partial_movie_file(self, hash_animation):
        hash_animation = '%03d_%s' % (len(self.partial_movie_files),
                                    hash_animation)

        super().add_partial_movie_file(hash_animation)

    def is_already_cached(self, hash_invocation):
        path = os.path.join(
            self.partial_movie_directory,
            "*_{}{}".format(hash_invocation, self.movie_file_extension),
        )
        return len(glob.glob(path)) != 0

    def close_movie_pipe(self):
        super().close_movie_pipe()

        this_path = Path(self.partial_movie_file_path)
        if '_uncached_' in this_path.name:
            return

        this_number = this_path.name[0:3]

        glob = '%s_*.mp4' % (this_number, )

        for cur_file in this_path.parent.glob(glob):
            if not cur_file.samefile(this_path):
                cur_file.unlink()
