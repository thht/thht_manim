from setuptools import setup

setup(
    name='thht_manim',
    version='0.0.1',
    packages=['thht_manim'],
    url='https://gitlab.com/thht/thht_manim',
    license='GPL3',
    author='Thomas Hartmann',
    author_email='thomas.hartmann@th-ht.de',
    description='Some manim extensions'
)
